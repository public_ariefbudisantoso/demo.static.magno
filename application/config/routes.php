<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';

$route['about'] = 'main/about';
$route['contact-us'] = 'main/about/contact';
$route['blogs'] = 'main/blog';
$route['blogs/detail'] = 'main/blog/detail';
$route['services'] = 'main/service';
$route['products'] = 'main/product';

$route['404_override'] = 'welcome/not_found';
$route['translate_uri_dashes'] = FALSE;
