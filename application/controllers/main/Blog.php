<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function index()
	{
		$datatemplate['title'] = "Index Blog | Magnoliving - Beyond network imagination";
        $datatemplate['body'] = 'body/blog/index';
        $this->load->view('template/mytemplate', $datatemplate);
	}

	public function detail($value = "")
	{
		// if (empty($value)) {
		// 	redirect("404_override");
		// }
		$datatemplate['title'] = "Detail Blog | Magnoliving - Beyond network imagination";
        $datatemplate['body'] = 'body/blog/detail';
        $this->load->view('template/mytemplate', $datatemplate);
	}
}
