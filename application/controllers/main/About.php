<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function index()
	{
		$datatemplate['title'] = "About Us | Magnoliving - Beyond network imagination";
        $datatemplate['body'] = 'body/about/index';
        $this->load->view('template/mytemplate', $datatemplate);
	}

	public function contact()
	{
		$datatemplate['title'] = "Contact Us | Magnoliving - Beyond network imagination";
        $datatemplate['body'] = 'body/about/contact';
        $this->load->view('template/mytemplate', $datatemplate);
	}

	public function newsletter()
	{
		print_r($_GET);die();
	}
}
