<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {

	public function index()
	{
		$datatemplate['title'] = "Services | Magnoliving - Beyond network imagination";
        $datatemplate['body'] = 'body/services/index';
        $this->load->view('template/mytemplate', $datatemplate);
	}
}
