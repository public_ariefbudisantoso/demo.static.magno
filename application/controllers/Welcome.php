<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$datatemplate['title'] = "Magnoliving - Beyond network imagination";
        $datatemplate['body'] = 'body/home/index';
        $this->load->view('template/mytemplate', $datatemplate);
	}

	public function not_found()
	{
		$datatemplate['title'] = "Page Not Found | Magnoliving - Beyond network imagination";
        $datatemplate['body'] = 'body/home/not_found';
        $this->load->view('template/mytemplate', $datatemplate);
	}
}
