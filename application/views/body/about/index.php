	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="codepixer">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Magnoliving</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="<?php echo site_url('assets/css/linearicons.css');?>">
			<link rel="stylesheet" href="<?php echo site_url('assets/css/font-awesome.min.css')?>">
			<link rel="stylesheet" href="<?php echo site_url('assets/css/bootstrap.css')?>">
			<link rel="stylesheet" href="<?php echo site_url('assets/css/magnific-popup.css')?>">
			<link rel="stylesheet" href="<?php echo site_url('assets/css/nice-select.css')?>">	
			<link rel="stylesheet" href="<?php echo site_url('assets/css/hexagons.min.css')?>">							
			<link rel="stylesheet" href="<?php echo site_url('assets/css/animate.min.css')?>">
			<link rel="stylesheet" href="<?php echo site_url('assets/css/owl.carousel.css')?>">
			<link rel="stylesheet" href="<?php echo site_url('assets/css/main.css')?>">
		</head>
		<body>	
			  <header id="header" id="home">
			    <div class="container main-menu">
			    	<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a href="<?php echo site_url();?>"><h3>Magnoliving</h3></a>
				      </div>
				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
				          <li class="menu-active"><a href="<?php echo site_url();?>">Home</a></li>
				          <li><a href="<?php echo site_url('about');?>">About Us</a></li>
				          <li><a href="<?php echo site_url('services');?>">Features</a></li>
				          <li class="menu-has-children"><a href="">Blog</a>
				            <ul>
				              <li><a href="<?php echo site_url('blogs');?>">Blog Home</a></li>
				              <li><a href="<?php echo site_url('blogs/detail');?>">Blog Single</a></li>
				            </ul>
				          </li>						          
				          <li><a href="<?php echo site_url('contact-us');?>">Contact</a></li>
				          				              
				        </ul>
				      </nav><!-- #nav-menu-container -->		    		
			    	</div>
			    </div>
			  </header><!-- #header -->

			  <!-- start banner Area -->
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								About Us				
							</h1>	
							<p class="text-white link-nav"><a href="<?php echo site_url();?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?php echo site_url('about');?>"> About Us</a></p>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->	

			<!-- Start about-video Area -->
			<section class="about-video-area section-gap">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-6 about-video-left">
							<h6 class="text-uppercase">Brand new app to blow your mind</h6>
							<h1>
								We’ve made a life <br>
								that will change you 
							</h1>
							<p>
								<span>We are here to listen from you deliver exellence</span>
							</p>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmo d tempor incididunt ut labore et dolore magna aliqua.
							</p>
							<a class="primary-btn" href="#">Get Started Now</a>
						</div>
						<div class="col-lg-6 about-video-right justify-content-center align-items-center d-flex relative">
							<div class="overlay overlay-bg"></div>
							<a class="play-btn" href="https://www.youtube.com/watch?v=ARA0AxrnHdM"><img class="img-fluid mx-auto" src="<?php echo site_url('assets/img/play-btn.png');?>" alt=""></a>
						</div>
					</div>
				</div>	
			</section>
			<!-- End about-video Area -->		

			<!-- Start home-about Area -->
			<section class="home-about-area">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-lg-6 home-about-left no-padding">
							<img class="mx-auto d-block img-fluid" src="<?php echo site_url('assets/img/about-img.png');?>" alt="">
						</div>
						<div class="col-lg-6 home-about-right no-padding">
							<h1>Globally Connected <br>
							by Large Network</h1>
							<h5>
								We are here to listen from you deliver exellence
							</h5>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.
							</p>
							<a class="primary-btn text-uppercase" href="#">Get Details</a>
						</div>
					</div>
				</div>	
			</section>
			<!-- End home-about Area -->

			<!-- Start brand Area -->
			<section class="brand-area pt-120">
				<div class="container">
					<div class="row align-items-center">
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l1.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l2.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l3.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l4.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l5.png');?>" alt="">
						</div>
					</div>
				</div>	
			</section>
			<!-- End brand Area -->

			
			<!-- start footer Area -->		
			<footer class="footer-area section-gap">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-12">
							<div class="single-footer-widget">
								<h6>Top Products</h6>
								<ul class="footer-nav">
									<li><a href="<?php echo site_url('about')?>">Magno Partner</a></li>
									<li><a href="#">Magno Advertising</a></li>
									<li><a href="<?php echo site_url('blogs')?>">Blog</a></li>
									<li><a href="https://play.google.com/store/apps/details?id=com.magno.mobile">Download</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-6  col-md-12">
							<div class="single-footer-widget newsletter">
								<h6>Newsletter</h6>
								<p>You can trust us. we only send promo offers, not a single spam.</p>
								<div id="mc_embed_signup">
									<form target="_blank" novalidate="true" action="<?php echo site_url('newsletter');?>" method="get" class="form-inline">

										<div class="form-group row" style="width: 100%">
											<div class="col-lg-8 col-md-12">
												<input name="EMAIL" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '" required="" type="email">
												<div style="position: absolute; left: -5000px;">
													<input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
												</div>
											</div> 
										
											<div class="col-lg-4 col-md-12">
												<button class="nw-btn primary-btn">Subscribe<span class="lnr lnr-arrow-right"></span></button>
											</div> 
										</div>		
										<div class="info"></div>
									</form>
								</div>		
							</div>
						</div>
						<div class="col-lg-3  col-md-12">
							<div class="single-footer-widget mail-chimp">
								<h6 class="mb-20">Instragram Feed</h6>
								<ul class="instafeed d-flex flex-wrap">
									<li><img src="<?php echo site_url('assets/img/i1.jpg');?>" alt=""></li>
									<li><img src="<?php echo site_url('assets/img/i2.jpg');?>" alt=""></li>
									<li><img src="<?php echo site_url('assets/img/i3.jpg');?>" alt=""></li>
									<li><img src="<?php echo site_url('assets/img/i4.jpg');?>" alt=""></li>
									<li><img src="<?php echo site_url('assets/img/i5.jpg');?>" alt=""></li>
									<li><img src="<?php echo site_url('assets/img/i6.jpg');?>" alt=""></li>
									<li><img src="<?php echo site_url('assets/img/i7.jpg');?>" alt=""></li>
									<li><img src="<?php echo site_url('assets/img/i8.jpg');?>" alt=""></li>
								</ul>
							</div>
						</div>						
					</div>

					<div class="row footer-bottom d-flex justify-content-between">
						<p class="col-lg-8 col-sm-12 footer-text m-0 text-white"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						<div class="col-lg-4 col-sm-12 footer-social">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-dribbble"></i></a>
							<a href="#"><i class="fa fa-behance"></i></a>
						</div>
					</div>
				</div>
			</footer>
			<!-- End footer Area -->	

			<script src="<?php echo site_url('assets/js/vendor/jquery-2.2.4.min.js')?>"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="<?php echo site_url('assets/js/vendor/bootstrap.min.js');?>"></script>			
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="<?php echo site_url('assets/js/easing.min.js')?>"></script>			
			<script src="<?php echo site_url('assets/js/hoverIntent.js')?>"></script>
			<script src="<?php echo site_url('assets/js/superfish.min.js')?>"></script>	
			<script src="<?php echo site_url('assets/js/jquery.ajaxchimp.min.js')?>"></script>
			<script src="<?php echo site_url('assets/js/jquery.magnific-popup.min.js')?>"></script>	
			<script src="<?php echo site_url('assets/js/owl.carousel.min.js')?>"></script>	
			<script src="<?php echo site_url('assets/js/hexagons.min.js')?>"></script>							
			<script src="<?php echo site_url('assets/js/jquery.nice-select.min.js')?>"></script>	
			<script src="<?php echo site_url('assets/js/jquery.counterup.min.js')?>"></script>
			<script src="<?php echo site_url('assets/js/waypoints.min.js')?>"></script>							
			<script src="<?php echo site_url('assets/js/mail-script.js')?>"></script>	
			<script src="<?php echo site_url('assets/js/main.js')?>"></script>	
		</body>
	</html>



