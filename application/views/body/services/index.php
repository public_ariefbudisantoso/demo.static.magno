	

			  <!-- start banner Area -->
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Features				
							</h1>	
							<p class="text-white link-nav"><a href="<?php echo site_url();?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?php echo site_url('services');?>"> Features</a></p>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->	

			<!-- Start offered-service Area -->
			<section class="offered-service-area section-gap">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="col-md-12 pb-40 header-text text-center">
							<h1 class="pb-10">Our Offered Services to you</h1>
							<p>
								Who are in extremely love with eco friendly system.
							</p>
						</div>
					</div>							
					<div class="row">
						<div class="col-lg-4">
							<div class="single-offered-service">
								<img class="img-fluid" src="<?php echo site_url('assets/img/os1.jpg');?>" alt="">
								<a href="#">
									<h4>Drone Architecture</h4>
								</a>	
								<p>
									inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct women face higher conduct.
								</p>
								
							</div>
						</div>
						<div class="col-lg-4">
							<div class="single-offered-service">
								<img class="img-fluid" src="<?php echo site_url('assets/img/os2.jpg');?>" alt="">
								<a href="#">
									<h4>Device Robot System</h4>
								</a>	
								<p>
									inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct women face higher conduct.
								</p>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="single-offered-service">
								<img class="img-fluid" src="<?php echo site_url('assets/img/os3.jpg');?>" alt="">
								<a href="#">
									<h4>Industrial Robot Management</h4>
								</a>	
								<p>
									inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct women face higher conduct.
								</p>
							</div>
						</div>												
					</div>
				</div>	
			</section>
			<!-- End offered-service Area -->
			

			<!-- Start feature Area -->
			<section class="feature-area section-gap">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="col-md-12 pb-40 header-text text-center">
							<h1 class="pb-10 text-white">Some Features that Made us Unique</h1>
							<p class="text-white">
								Who are in extremely love with eco friendly system.
							</p>
						</div>
					</div>							
					<div class="row">
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-file-empty"></span>
									<h4>Billing & Transaction</h4>
								</a>
								<p>
									Check utility bills info & reminder. Track payment status
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-license"></span>
									<h4>News & Updates</h4>
								</a>
								<p>
									Receive important announcements straight to your smartphone. Elevator maintenance schedule, fire drill info, and others!
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-phone"></span>
									<h4>Tenant Relations</h4>
								</a>
								<p>
									Report incidents and track follow ups, book facility, contact your building management from anywhere
								</p>
							</div>
						</div>						
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-store"></span>
									<h4>Shop Daily Needs</h4>
								</a>
								<p>
									Explore shops in the area and order straight from the app. Water & gas, groceries, F&B, laundry, and many more!
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-camera-video"></span>
									<h4>GuestCam</h4>
								</a>
								<p>
									View live CCTV of the building's public area, verify your guests before granting access to your unit.
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-bubble"></span>
									<h4>MagnoPay</h4>
								</a>
								<p>
									Pay for bills and purchases through Magno's in-app e-wallet
								</p>
							</div>
						</div>	

					</div>
				</div>	
			</section>
			<!-- End feature Area -->			
				
			<!-- Start brand Area -->
			<section class="brand-area pt-120">
				<div class="container">
					<div class="row align-items-center">
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l1.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l2.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l3.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l4.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l5.png');?>" alt="">
						</div>
					</div>
				</div>	
			</section>
			<!-- End brand Area -->

			

