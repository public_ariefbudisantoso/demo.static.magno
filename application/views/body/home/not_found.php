			<!-- start banner Area -->
			<section class="banner-area" id="home">
				<div class="container">
					<div class="row fullscreen d-flex align-items-center justify-content-center">
						<div class="banner-content col-lg-6 col-md-6">
							<h1>
								404 <br>
								Page Not Found <br>
							</h1>
							<p class="text-white text-uppercase"></p>
							<a href="javascript:void(0)" onClick="javascript:history.go(-1)" class="primary-btn header-btn text-uppercase">Back to Page</a>
						</div>
						<div class="banner-img col-lg-6 col-md-6">
							<center><img class="img-fluid" src="<?php echo site_url('assets/img/galaxy_s5_port_white.png');?>" style="width: 45%" alt=""></center>
						</div>												
					</div>
				</div>
			</section>
			<!-- End banner Area -->