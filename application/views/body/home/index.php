
			<!-- start banner Area -->
			<section class="banner-area" id="home">
				<div class="container">
					<div class="row fullscreen d-flex align-items-center justify-content-center">
						<div class="banner-content col-lg-6 col-md-6">
							<h1>
								Improved <br>
								Convenience level <br>
								with <b style="color: black;">Magno</b>
							</h1>
							<p class="text-white text-uppercase">
								Everyone wanna live in an apartment with just fingertips
							</p>
							<a href="<?php echo site_url('services');?>" class="primary-btn header-btn text-uppercase">View Details</a>
						</div>
						<div class="banner-img col-lg-6 col-md-6">
							<center><img class="img-fluid" src="<?php echo site_url('assets/img/galaxy_s5_port_white.png');?>" style="width: 45%" alt=""></center>
						</div>												
					</div>
				</div>
			</section>
			<!-- End banner Area -->


			<!-- Start home-about Area -->
			<section class="home-about-area">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-lg-6 home-about-left no-padding">
							<img class="mx-auto d-block img-fluid" src="<?php echo site_url('assets/img/about-img.png');?>" alt="">
						</div>
						<div class="col-lg-6 home-about-right no-padding">
							<h1>Globally Connected <br>
							by Large Network</h1>
							<h5>
								We are here to listen from you deliver exellence
							</h5>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.
							</p>
							<a class="primary-btn text-uppercase" href="https://play.google.com/store/apps/details?id=com.magno.mobile">Get Details</a>
						</div>
					</div>
				</div>	
			</section>
			<!-- End home-about Area -->
			

			<!-- Start about-video Area -->
			<section class="about-video-area section-gap">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-6 about-video-left">
							<h6 class="text-uppercase">Brand new app to blow your mind</h6>
							<h1>
								We’ve made a life <br>
								that will change you 
							</h1>
							<p>
								<span>We are here to listen from you deliver exellence</span>
							</p>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmo d tempor incididunt ut labore et dolore magna aliqua.
							</p>
							<a class="primary-btn" href="https://play.google.com/store/apps/details?id=com.magno.mobile">Get Started Now</a>
						</div>
						<div class="col-lg-6 about-video-right justify-content-center align-items-center d-flex relative">
							<div class="overlay overlay-bg"></div>
							<a class="play-btn" href="https://www.youtube.com/watch?v=ARA0AxrnHdM"><img class="img-fluid mx-auto" src="<?php echo site_url('assets/img/play-btn.png');?>" alt=""></a>
						</div>
					</div>
				</div>	
			</section>
			<!-- End about-video Area -->
			

			<!-- Start feature Area -->
			<section class="feature-area section-gap">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="col-md-12 pb-40 header-text text-center">
							<h1 class="pb-10 text-white">Some Features that Made us Unique</h1>
							<p class="text-white">
								Who are in extremely love with eco friendly system.
							</p>
						</div>
					</div>							
					<div class="row">
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-file-empty"></span>
									<h4>Billing & Transaction</h4>
								</a>
								<p>
									Check utility bills info & reminder. Track payment status
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-license"></span>
									<h4>News & Updates</h4>
								</a>
								<p>
									Receive important announcements straight to your smartphone. Elevator maintenance schedule, fire drill info, and others!
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-phone"></span>
									<h4>Tenant Relations</h4>
								</a>
								<p>
									Report incidents and track follow ups, book facility, contact your building management from anywhere
								</p>
							</div>
						</div>						
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-store"></span>
									<h4>Shop Daily Needs</h4>
								</a>
								<p>
									Explore shops in the area and order straight from the app. Water & gas, groceries, F&B, laundry, and many more!
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-camera-video"></span>
									<h4>GuestCam</h4>
								</a>
								<p>
									View live CCTV of the building's public area, verify your guests before granting access to your unit.
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="single-feature">
								<a href="#" class="title d-flex flex-row">
									<span class="lnr lnr-bubble"></span>
									<h4>MagnoPay</h4>
								</a>
								<p>
									Pay for bills and purchases through Magno's in-app e-wallet
								</p>
							</div>
						</div>	

					</div>
				</div>	
			</section>
			<!-- End feature Area -->
			

			<!-- Start brand Area -->
			<section class="brand-area pt-120">
				<div class="container">
					<div class="row align-items-center">
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l1.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l2.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l3.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l4.png');?>" alt="">
						</div>
						<div class="col single-brand">
							<img class="img-fluid" src="<?php echo site_url('assets/img/l5.png');?>" alt="">
						</div>
					</div>
				</div>	
			</section>
			<!-- End brand Area -->
			

			<!-- Start blog Area -->
			<section class="blog-area section-gap">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="col-md-12 pb-40 header-text text-center">
							<h1 class="pb-10">Latest News from our Blog</h1>
							<p>
								Who are in extremely love with eco friendly system.
							</p>
						</div>
					</div>						
					<div class="row">
						<div class="col-lg-6 col-md-6 blog-left">
							<div class="thumb">
								<img class="img-fluid" src="<?php echo site_url('assets/img/b1.jpg');?>" alt="">
							</div>
							<div class="detais">
								<ul class="tags">
									<li><a href="#">Travel</a></li>
									<li><a href="#">Life Style</a></li>
								</ul>
								<a href="<?php echo site_url('blogs/detail')?>"><h4>Portable latest Fashion for young women</h4></a>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore.
								</p>
								<p class="date">31st January, 2018</p>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 blog-right">
							<div class="thumb">
								<img class="img-fluid" src="<?php echo site_url('assets/img/b2.jpg');?>" alt="">
							</div>
							<div class="detais">
								<ul class="tags">
									<li><a href="#">Travel</a></li>
									<li><a href="#">Life Style</a></li>
								</ul>
								<a href="<?php echo site_url('blogs/detail')?>"><h4>Portable latest Fashion for young women</h4></a>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore.
								</p>
								<p class="date">31st January, 2018</p>
							</div>							
						</div>
					</div>
				</div>	
			</section>
			<!-- End blog Area -->
			


